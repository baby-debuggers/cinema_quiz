from django.db import models

# Create your models here.

class Soal(models.Model):
    question = models.TextField(max_length=250)
    gozineha = models.TextField(max_length=250)
    answer = models.CharField(max_length=1)  # گزینه های صحیح
    shomaresoal = models.IntegerField()  # از ایدی هم میشد استفاده کرد ولی اگه یک سوال پاک میشد ترتیب سوال ها به هم میریخت


    def __str__(self):
        return self.question # سوال ها رو نشون میده تو پنل ادمین