from django.urls import path
from soal import views

urlpatterns = [
    path('',views.welcome),    # یه صفحه ساده معرفی که نیازی هم نبود
    path('daryaft',views.ersal), # چون کاربر می خواد سوالا رو دریافت کنه یوارال دریافته ولی ما می خوایمم ارسال کنیم تابع ویو ارساله رو داریم
    path('ersal',views.daryaft), # همون بالایی ولی برعکس
]