from django.shortcuts import render
from .models import Soal
from django.http import JsonResponse , HttpResponse
import json
from django.views.decorators.csrf import csrf_exempt




def welcome(request):
    '''
    یک صفحه خوش امد گویی
    '''

    return HttpResponse('<h1 style="color:red; text-align: center; font-size: 100px; line-height: 500px; ">به سامانه آزمون آنلاین فیلم و سریال خوش آمدید</h1>')




def ersal(request):
    '''
    ersal soalat baraye karbar be sorat json

    '''
    if request.method == 'GET':
        soalat = Soal.objects.all()
        
        json_res = []
        for soal in soalat:
            temp = {
                'soal {}'.format(soal.shomaresoal) : soal.question , 
                'gozineha' : soal.gozineha ,
            }
            json_res.append(temp)

        return JsonResponse(json_res, safe=False)

    return HttpResponse("Bad request")





@csrf_exempt
def daryaft(request):
    '''
    daryaft javabha be sort json va bargardondane nomre
    '''
    if request.method == "POST":
        data = request.body
        answer = json.loads(data)

        correct = 0
        wrong = 0
        total = 20
        # q = shomaresoal va a = javab (to json)
        try:
            for q , a in answer.items():
                the_question = Soal.objects.get(shomaresoal = q)
                if the_question.answer == a :  
                    correct += 1
                elif a:
                    wrong += 1 
            
            score = ((correct*3)-wrong)/(total*3)*100            
            return JsonResponse({"score": score})
        except:
            return JsonResponse({"status":"bad request"})
    

        