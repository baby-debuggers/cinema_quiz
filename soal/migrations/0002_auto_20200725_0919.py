# Generated by Django 3.0.8 on 2020-07-25 09:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('soal', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='soal',
            name='gozineha',
            field=models.TextField(max_length=250),
        ),
        migrations.AlterField(
            model_name='soal',
            name='question',
            field=models.TextField(max_length=250),
        ),
    ]
